package com.springboot.example.web;

import com.alibaba.dubbo.config.annotation.Reference;
import com.springboot.example.domain.User;
import com.springboot.example.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.io.IOException;

/**
 * Created by masong on 2017/7/28.
 */
@RestController
public class UserController {

    @Reference
    UserService userService;


    @RequestMapping(method = RequestMethod.GET, value = "/{id}")
    public User find(@PathVariable("id") Long id) throws IOException {
        return userService.find(id);
    }

    @RequestMapping(method = RequestMethod.GET, value = "/queryByName")
    public User queryByName(@RequestParam("id") Long id) throws IOException {
        return userService.find(id);
    }
}
