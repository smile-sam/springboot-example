package com.springboot.example.domain;

import java.io.Serializable;

/**
 * Created by masong on 2017/7/28.
 */
public class User implements Serializable {

    private Long id;
    private String name;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
