package com.springboot.example.service;

import com.springboot.example.domain.User;

/**
 * Created by masong on 2017/7/28.
 */
public interface UserService {

    public User find(Long id);

    public User find(String name);

}
