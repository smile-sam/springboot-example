package com.springboot.example.service.impl;

import com.alibaba.dubbo.config.annotation.Service;
import com.springboot.example.domain.User;
import com.springboot.example.service.UserService;

/**
 * Created by masong on 2017/7/28.
 */
@Service
public class UserServiceImpl implements UserService {
    @Override
    public User find(Long id) {
        User user = new User();
        user.setId(1L);
        user.setName("张三");
        return user;
    }

    @Override
    public User find(String name) {
        User user = new User();
        user.setId(2L);
        user.setName("李四");
        return user;
    }
}
